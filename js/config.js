'use strict';
app

.constant('config', { 

  urlBase: 'http://appcelerate.net.au/cityguide',
  currency: '$',
  onesignalId: '52ed65ee-3aaf-492e-ab07-cdd8e65772ef',
  playstoreAppId: 'YOUR_PLAYSTORE_APP_ID',
  appstoreAppId: 'YOUR_APPSTORE_ID',
  playstoreAppUrl: 'https://play.google.com/store/apps/details?id=',
  appstoreAppUrl: 'https://itunes.apple.com/in/app/',

});